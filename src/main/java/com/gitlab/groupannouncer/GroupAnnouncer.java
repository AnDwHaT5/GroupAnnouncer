/* Copyright (C) Jul 22, 2017 Avery Gambetti - All Rights Reserved
 * This software may not be copied, modified, or distributed beyond
 * permitted sources as defined by the author.
 */
package com.gitlab.groupannouncer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import com.gitlab.groupannouncer.commands.ExecutorGroupBroadcast;
import com.google.inject.Inject;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Group;
@Plugin(id="groupannouncer", name="GroupAnnouncer", authors="AnDwHaT5", version="1.0.0", dependencies = {
        @Dependency(id = "luckperms")}, description="Broadcasts a message to a certain group.")
public class GroupAnnouncer {
	
	@Inject
	Game game;
	
	@Inject
	Logger logger;
	
	@Listener
	public void gameInit(GameInitializationEvent e)
	{
		Set<Group> groupset = LuckPerms.getApi().getGroups();
		Map<String, String> groups = new HashMap<>();
		for(Group group : groupset)
			groups.put(group.getName(), group.getName());
		CommandSpec groupBroadcaster = CommandSpec.builder().executor(new ExecutorGroupBroadcast())
				.description(Text.of("Sends a message to all members of a certain group."))
				.arguments(GenericArguments.choices(Text.of("group"), groups , false), 
						GenericArguments.remainingJoinedStrings(Text.of("message")))
				.permission("groupannouncer.groupbroadcast")
				.build();
		Sponge.getCommandManager().register(this, groupBroadcaster, "groupbroadcaster", "gbc");
	}

}
