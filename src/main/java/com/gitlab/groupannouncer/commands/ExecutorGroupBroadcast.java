/* Copyright (C) Jul 22, 2017 Avery Gambetti - All Rights Reserved
 * This software may not be copied, modified, or distributed beyond
 * permitted sources as defined by the author.
 */
package com.gitlab.groupannouncer.commands;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Group;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.common.api.delegates.UserDelegate;

public class ExecutorGroupBroadcast implements CommandExecutor{
	
	private UserDelegate getUser(UUID uuid)
	{
		return (UserDelegate) lpa.getUserSafe(uuid).orElse(null);
	}
	
	private void sendMessageToGroup(Group group, String message, String sender)
	{
		Collection<Player> players = Sponge.getServer().getOnlinePlayers();
		for(Player p : players)
		{
			UserDelegate ud = getUser(p.getUniqueId());
			if(ud.getGroupNames().contains(group.getName()))
			{
				p.sendMessage(Text.of(TextColors.LIGHT_PURPLE, "[", TextColors.AQUA, sender, 
						TextColors.LIGHT_PURPLE, "->", TextColors.AQUA, group.getName(), 
						TextColors.LIGHT_PURPLE,"] ", TextColors.AQUA, message));
			}
		}
	}
	
	final LuckPermsApi lpa = Sponge.getServiceManager().getRegistration(LuckPermsApi.class).get().getProvider();

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		Set<Group> groups = LuckPerms.getApi().getGroups();

		if(args.getOne("group").isPresent())
		{
			String grouptob = args.getOne("group").get().toString();
			Group g = LuckPerms.getApi().getGroup(grouptob);
			if(groups.contains(g))
			{
				if(args.getOne("message").isPresent())
				{
					sendMessageToGroup(g, args.getOne("message").get().toString(), (src instanceof Player ? src.getName():"Console")+"");
				}
			}
			else
			{
				src.sendMessage(Text.of(TextColors.LIGHT_PURPLE, "[GroupAnnouncer]", TextColors.AQUA, "That group does not exist!"));
			}
		}
		else
		{
			
			String groupsstring = "";
			if(groups.size() == 1)
			{
				groupsstring += ((Group)groups.toArray()[0]).getName()+".";
			}
			else
			{
				for(int i = 0; i < groups.size(); i++)
				{
					if(i == groups.size()-1)
					{
						groupsstring += "and "+((Group)groups.toArray()[i]).getName()+".";
					}
					else
					{
						groupsstring += ((Group)groups.toArray()[i]).getName()+", ";
					}
				}
			}
			src.sendMessage(Text.of(TextColors.LIGHT_PURPLE, "[GroupAnnouncer]", TextColors.AQUA, " Incorrect Usage: /GroupBroadcast Group:GroupList Message. "
					+ "Available groups are: "+groupsstring));
			return CommandResult.success();
		}
		return CommandResult.success();
	}

	
}